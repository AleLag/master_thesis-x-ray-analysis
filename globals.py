"""
File for storing all global variables: flags, paths, arm parts, ranges for arm parts
"""
ACTION = True
ORIGINAL_DATASET_DIR = "C:\\Users\\Aleksander Lagierski\\Documents\\STUDIA MGR\\STUDIA MGR\\MGR\\xray_analysis\\dataset\\train"
BASE_DIR = "C:\\Users\\Aleksander Lagierski\\Documents\\STUDIA MGR\\STUDIA MGR\\MGR\\xray_analysis\\mgr_xray\\master_thesis-x-ray-analysis\\xray_dataset"
ARM_PARTS = ['elbow', 'finger', 'forearm', 'hand', 'humerus', 'shoulder', 'wrist']
RANGES_ELBOW = [(0, 1000), (1000, 1500), (1500, 2000)] # 1000 train, 500 val, 500 test
RANGES_FINGER = [(0, 1000), (1000, 1500), (1500, 2000)] # 1000 train, 500 val, 500 test
RANGES_FOREARM = [(0, 400), (400, 600), (600, 800)] # 400 train, 200 val, 200 test
RANGES_HAND = [(0, 650), (650, 975), (975, 1300)] # 650 train, 325 val, 325 test
RANGES_HUMERUS = [(0, 300), (300, 450), (450, 600)] #300 train, 150 val, 150 test
RANGES_SHOULDER = [(0, 2000), (2000, 3000), (3000, 4000)] #2000 train, 1000val, 1000 test
RANGES_WRIST = [(0, 2000), (2000, 3000), (3000, 4000)] #2000 train, 1000val, 1000test
RANGES = [RANGES_ELBOW, RANGES_FINGER, RANGES_FOREARM, RANGES_HAND, RANGES_HUMERUS, RANGES_SHOULDER, RANGES_WRIST]
PATHS_DICTIONARY = {
    "ORIGINAL_DATASET_PATH": ORIGINAL_DATASET_DIR
}
BASE_MAP= {
    "abnormal": "positive",
    "normal": "negative"
}