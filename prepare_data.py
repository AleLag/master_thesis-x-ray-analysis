import os
import re
import glob
import shutil
from globals import ACTION, BASE_DIR, ORIGINAL_DATASET_DIR, ARM_PARTS, RANGES, PATHS_DICTIONARY, BASE_MAP

def prepare_root_directories(action=ACTION, base_dir=BASE_DIR):
    """
    Create root directories (train, validation, test) under base_dir.
    """
    if action:
        os.mkdir(base_dir)
    train_dir = os.path.join(base_dir, "train")
    validation_dir = os.path.join(base_dir, "validation")
    test_dir = os.path.join(base_dir, "test")
    if action:
        for directory in [train_dir, validation_dir, test_dir]:
            os.mkdir(directory)
    return [train_dir, validation_dir, test_dir]

def create_arm_part_subdirectories(arm_part, root_subdirectory, action=ACTION):
    """
    Create arm part directories under root_subdirectory (train, validation, test).
    """
    subdirectory = os.path.join(root_subdirectory, arm_part)
    if action:  os.mkdir(subdirectory)
    normal_dir = os.path.join(subdirectory, 'normal')
    abnormal_dir = os.path.join(subdirectory, 'abnormal')
    if action:
        os.mkdir(normal_dir)
        os.mkdir(abnormal_dir)
    return subdirectory, normal_dir, abnormal_dir

def move_data2subdiectory(arm_part, subdirectory, ran, original_dataset=ORIGINAL_DATASET_DIR):
    """
    Copy the image files to specific subdirectory (based on arm part and abnormality). Amount and list of files are specified by range.
    """
    abnormality = os.path.basename(subdirectory)
    orginal_arm_part_subset = os.path.join(original_dataset, f'XR_{arm_part.upper()}')
    list_of_files_of_given_abnormality = glob.glob(orginal_arm_part_subset + f'//patient*//study*_{BASE_MAP[abnormality]}//*.png')
    for index, _file in enumerate(list_of_files_of_given_abnormality[ran[0]: ran[1]]):
        file_to_copy = convert_file_name(_file, abnormality, index)
        file_to_copy = os.path.join(subdirectory, file_to_copy)
        shutil.copy2(_file, file_to_copy)


    
def extract_patient_number(file_path):
    """
    Method for extraction patient number from file name.
    """
    re_patient_number = re.compile(r'.*\\patient(?P<number>\d+).*')
    return re.search(re_patient_number, file_path).group('number')

def extract_study_number(file_path):
    """
    Method for extraction study number from file name.
    """
    re_study_number = re.compile(r'.*\\study(?P<number>\d+).*')
    return re.search(re_study_number, file_path).group('number')

def convert_file_name(file_path, abnormality, index):
    """
    Method for creation of new file name with usage of patient number, study number and index number (index of the element from slice).
    """
    patient_number = extract_patient_number(file_path)
    study_number = extract_study_number(file_path)
    new_file_name = f'{abnormality}_patient{patient_number}_{study_number}_{index}.png'
    return new_file_name

def runner(action=ACTION):
    """
    Function that performs data preparation.
    """
    all_ranges = []
    for i in range(3):
        all_ranges.append([ran[i] for ran in RANGES])
    root_subdirectories = prepare_root_directories(action=action)
    for subdir, set_ranges in zip(root_subdirectories, all_ranges):
        subset = os.path.basename(subdir).upper()
        PATHS_DICTIONARY[f'{subset}_DIRECTORY_PATH'] = subdir
        for arm_part, arm_range in zip(ARM_PARTS, set_ranges):
            subdirectory, normal_dir, abnormal_dir = create_arm_part_subdirectories(arm_part=arm_part, root_subdirectory=subdir, action=action)
            PATHS_DICTIONARY[f'{subset}_{arm_part.upper()}_DIRECTORY_PATH'] = subdirectory
            PATHS_DICTIONARY[f'{subset}_{arm_part.upper()}_NORMAL_DIRECTORY_PATH'] = normal_dir
            PATHS_DICTIONARY[f'{subset}_{arm_part.upper()}_ABNORMAL_DIRECTORY_PATH'] = abnormal_dir
            if action:
                move_data2subdiectory(arm_part=arm_part, subdirectory=normal_dir, ran=arm_range, original_dataset=PATHS_DICTIONARY['ORIGINAL_DATASET_PATH'])
                move_data2subdiectory(arm_part=arm_part, subdirectory=abnormal_dir, ran=arm_range, original_dataset=PATHS_DICTIONARY['ORIGINAL_DATASET_PATH'])
    return PATHS_DICTIONARY
