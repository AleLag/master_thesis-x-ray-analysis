import os
import numpy as np
import tensorflow as tf
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from misc import get_img_array

keras = tf.keras
layers = keras.layers

class Visualizer:

    def __init__(self, model_path, body_part, dataset_dir) -> None:
        self.model = keras.models.load_model(model_path)
        dataset_path = dataset_dir['TEST_{}_ABNORMAL_DIRECTORY_PATH'.format(body_part.upper())]
        #TODO create method for getting the most certain image
        img_path = os.listdir(dataset_path)[236]
        print(img_path)
        self.img_path = os.path.join(dataset_path, img_path)
        self.img_tensor = get_img_array(img_path=self.img_path, target_size=(180, 180))
    
    def show_summary(self):
        return self.model.summary()

    def visualize_intermediate_activations(self):
        layer_outputs = []
        layer_names = []

        for layer in self.model.layers:
            if isinstance(layer, (layers.Conv2D, layers.MaxPooling2D)):
                layer_outputs.append(layer.output)
                layer_names.append(layer.name)
        
        activation_model = keras.Model(self.model.inputs, layer_outputs)
        activations = activation_model.predict(self.img_tensor)
        # complete visualization of all activations in the network

        imagages_per_row = 16
        for layer_name, layer_activation in zip(layer_names, activations):
            n_feature = layer_activation.shape[-1]
            size = layer_activation.shape[1]
            n_cols = n_feature // imagages_per_row
            display_grid = np.zeros(
                ((size + 1) * n_cols - 1,
                imagages_per_row * (size + 1) -1)
            )
            for col in range(n_cols):
                for row in range(imagages_per_row):
                    channel_index = col * imagages_per_row + row
                    channel_image = layer_activation[0, :, :, channel_index].copy()
                    if channel_image.sum() != 0:
                        channel_image -= channel_image.mean()
                        channel_image /= channel_image.std()
                        channel_image *= 64
                        channel_image += 128
                    channel_image = np.clip(channel_image, 0, 255).astype('uint8')
                    display_grid[
                        col * (size + 1): (col + 1) * size + col,
                        row * (size + 1): (row + 1) * size + row
                    ] = channel_image
            scale = 1./size
            plt.figure(figsize=(scale * display_grid.shape[1], scale * display_grid.shape[0]))
            plt.title(layer_name)
            plt.grid(False)
            plt.axis('off')
            plt.imshow(display_grid, aspect='auto', cmap='viridis')
        plt.show()

    def visualize_covnet_filters(self, layer_name):
        layer = self.model.get_layer(name=layer_name)
        feature_extractor = keras.Model(self.model.inputs, layer.output)
        img_width = 180
        img_height = 180
        all_images = []
        for filter_index in range(64):
            image = deprocess_image(
                generate_filter_pattern(filter_index, img_width, img_height, feature_extractor)
            )
            all_images.append(image)
        margin = 5
        n = 8
        cropped_width = img_width - 25 * 2
        cropped_height = img_width - 25 * 2
        width = n * cropped_width + (n - 1) * margin
        height = n * cropped_height + (n - 1) * margin
        stitched_filters = np.zeros((width, height, 3))

        for i in range(n):
            for j in range(n):
                image = all_images[i * n +j]
                row_start = (cropped_width + margin) * i
                row_end = (cropped_width + margin) * i + cropped_width
                column_start = (cropped_height +  margin) * j
                column_end = (cropped_height +  margin) * j + cropped_height

                stitched_filters[
                    row_start:row_end,
                    column_start:column_end,
                    :
                ] = image
        keras.utils.save_img(f'{self.body_part}_filters_for_layer_{layer_name}.png', stitched_filters)
        return True

    def heatmap(self, superimposed_file_name):
        print(self.model.predict(self.img_tensor))
        for layer in self.model.layers[::-1]:
            if layer.name.startswith('dense'):
                dense_layer = layer.name
                break
        print(dense_layer)
        for layer in self.model.layers[::-1]:
            if layer.name.startswith('conv2d') or layer.name.startswith('separable_conv2d'):
                last_conv_layer_name = layer.name
                break
        print(last_conv_layer_name)
        classifier_layer_names = [
            "global_average_pooling2d",
            dense_layer
        ]
        last_conv_layer = self.model.get_layer(last_conv_layer_name)
        last_conv_layer_model = keras.Model(self.model.input, last_conv_layer.output)

        classifier_input = keras.Input(shape=last_conv_layer.output.shape[1:])
        x = classifier_input
        for layer_name in classifier_layer_names:
            x = self.model.get_layer(layer_name)(x)
        classifier_model = keras.Model(classifier_input, x)

        with tf.GradientTape() as tape:
            last_conv_layer_output = last_conv_layer_model(self.img_tensor)
            tape.watch(last_conv_layer_output)
            preds = classifier_model(last_conv_layer_output)
            top_pred_index = tf.argmax(preds[0])
            top_class_channel = preds[:, top_pred_index]

        grads = tape.gradient(top_class_channel, last_conv_layer_output)
        pooled_grads = tf.reduce_mean(grads, axis=(0,1,2))
        last_conv_layer_output = last_conv_layer_output[0]

        heatmap = last_conv_layer_output @ pooled_grads[..., tf.newaxis]
        heatmap = tf.squeeze(heatmap)
        heatmap = tf.maximum(heatmap, 0) / tf.math.reduce_max(heatmap)

        img = keras.preprocessing.image.load_img(self.img_path)
        img = keras.preprocessing.image.img_to_array(img)
        heatmap = np.uint8(255 * heatmap)

        jet = cm.get_cmap("jet")
        jet_colors = jet(np.arange(256))[:, :3]
        jet_heatmap = jet_colors[heatmap]

        jet_heatmap = keras.preprocessing.image.array_to_img(jet_heatmap)
        jet_heatmap = jet_heatmap.resize((img.shape[1], img.shape[0]))
        jet_heatmap = keras.preprocessing.image.img_to_array(jet_heatmap)

        superimposed_img = jet_heatmap * 0.4 + img
        superimposed_img = keras.preprocessing.image.array_to_img(superimposed_img)
        superimposed_img.save(superimposed_file_name)

        return True
    
def compute_loss(image, filter_index, feature_extractor):
    activation = feature_extractor(image)
    filter_activation = activation[:, 2:-2, 2:-2, filter_index]
    return tf.reduce_mean(filter_activation)


@tf.function
def gradient_ascent_step(image, filter_index, learning_rate, feature_extractor):
    with tf.GradientTape() as tape:
        tape.watch(image)
        loss = compute_loss(image, filter_index, feature_extractor)
    grads = tape.gradient(loss, image)
    grads = tf.math.l2_normalize(grads)
    image += learning_rate * grads
    return image

def generate_filter_pattern(filter_index, img_width, img_height, feature_extractor):
    iterations = 30
    learning_rate = 10
    image = tf.random.uniform(
        minval=0.4,
        maxval=0.6,
        shape=(1, img_width, img_height, 3)
    )
    for _ in range(iterations):
        image = gradient_ascent_step(image, filter_index, learning_rate, feature_extractor)
    return image[0].numpy()

def deprocess_image(image):
    image -= image.mean()
    image /= image.std()
    image *= 64
    image += 128
    image = np.clip(image, 0, 255).astype('uint8')
    image = image[25:-25, 25:-25, :]
    return image