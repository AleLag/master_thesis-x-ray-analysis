import tensorflow as tf
from collections import namedtuple
from misc import get_data_augmentation_layer, get_dataset, get_callbacks, draw_plots

keras = tf.keras
layers = keras.layers
apps = keras.applications
KerasApplication = namedtuple('KerasApplication', ['model', 'preprocess'])

CONV_BASE_MAP = {
    "vgg16": KerasApplication(apps.vgg16.VGG16, apps.vgg16.preprocess_input),
    "xception": KerasApplication(apps.xception.Xception, apps.xception.preprocess_input)
    # will add some more
}

class XRayAnalysisModel:

    def __init__(self, body_part, dataset_dir, conv_base_model=None) -> None:
        self.body_part = body_part
        self.main_dataset = dataset_dir
        self.model = None
        self.train_dataset = self.init_train_dataset()
        self.test_dataset = self.init_test_dataset()
        self.val_dataset = self.init_validation_dataset()
        self.conv_base = None
        self.history = None
        if conv_base_model is not None:
            self.conv_base_model = CONV_BASE_MAP[conv_base_model.lower()].model
            self.preprocess = CONV_BASE_MAP[conv_base_model.lower()].preprocess
            self.init_convbase()
    
    def show_model_summary(self):
        return self.model.summary()

    def init_convbase(self, weights='imagenet'):
        self.conv_base = self.conv_base_model(
            weights=weights,
            include_top = False
        )
        return True

    def init_model(self, data_augmentation=False, feature_extration=False, fine_tunning=False):
        inputs = keras.Input(shape=(180, 180, 3))

        # IF DATA AUGMENTATION
        if data_augmentation:
            x = self.data_augmentation()(inputs)
        else:
            x = inputs
        
        # IF FEATURE EXTRACTION
        if feature_extration:
            x = self.feature_extration(x)
        
        # IF FINE TUNNING
        if fine_tunning:
            x = self.fine_tunning(x)

        if not any([feature_extration, fine_tunning]):
            x = layers.Rescaling(1./255)(x)
            x = layers.Conv2D(filters=32, kernel_size=5, use_bias=False)(x)

        for size in [32, 64]:
            x = self.get_residual_block(x, filters=size, pooling=True)
        x = self.get_residual_block(x, 128, pooling=False)

        x = layers.GlobalAveragePooling2D()(x)
        x = layers.Dense(128)(x)
        x = layers.Dropout(0.5)(x)
        outputs = layers.Dense(1, activation='sigmoid')(x)
        self.model = keras.Model(inputs, outputs)
        return True
            

    def compile_model(self):
        if self.model is None:
            self.init_model()
        return self.model.compile(
            loss='binary_crossentropy',
            optimizer=keras.optimizers.RMSprop(learning_rate=1e-5),
            metrics=['accuracy']
        )

    def _prepare_dataset(self, set_dir: str, img_size: tuple, batch_size: int):
        return get_dataset(dir=set_dir, image_size=img_size, batch_size=batch_size)

    def init_test_dataset(self):
        return self._prepare_dataset(
            set_dir=self.main_dataset['TEST_{}_DIRECTORY_PATH'.format(self.body_part.upper())],
            img_size=(180, 180),
            batch_size=32
        )

    def init_validation_dataset(self):
        return self._prepare_dataset(
            set_dir=self.main_dataset['VALIDATION_{}_DIRECTORY_PATH'.format(self.body_part.upper())],
            img_size=(180, 180),
            batch_size=32
        )

    def init_train_dataset(self):
        return self._prepare_dataset(
            set_dir=self.main_dataset['TRAIN_{}_DIRECTORY_PATH'.format(self.body_part.upper())],
            img_size=(180, 180),
            batch_size=32
        )

    def fit_model(self, epochs: int, save_model_path: str):
        if self.model is None:
            self.init_model()
            self.compile_model()
        self.history = self.model.fit(
            self.train_dataset,
            epochs=epochs,
            validation_data=self.val_dataset,
            callbacks=self._get_callbacks(filepath=save_model_path)
        ).history
        return True
        
    def evaluate_model(self, filepath) -> tuple:
        test_model = keras.models.load_model(filepath)
        return test_model.evaluate(self.test_dataset)

    def graph(self):
        if self.history is not None:
            return draw_plots(self.history)
        else:
            print('You have not fitted the model yet')
            return False

    def fine_tunning(self, x, tunning_layers=4):
        self.conv_base.trainable = True
        for layer in self.conv_base.layers[:-tunning_layers]: # to verify
            layer.trainable = False
        return self.feature_extraction(x, freeze_base=False)
        

    def feature_extraction(self, x, freeze_base=True):
        if self.conv_base is None:
            self.init_convbase()
        if freeze_base:
            self.conv_base.trainable = False
        x = self.preprocess(x)
        x = self.conv_base(x)
        return x

    def data_augmentation(self):
        return get_data_augmentation_layer()

    def get_residual_block(self, x, filters, pooling=False):
        residual = x

        x = layers.BatchNormalization()(x)
        x = layers.Activation('relu')(x)
        x = layers.SeparableConv2D(filters=filters, kernel_size=3, padding='same', use_bias=False)(x)

        x = layers.BatchNormalization()(x)
        x = layers.Activation('relu')(x)
        x = layers.SeparableConv2D(filters=filters, kernel_size=3, padding='same', use_bias=False)(x)

        if pooling:
            x = layers.MaxPooling2D(2, padding='same')(x)
            residual = layers.Conv2D(filters, 1, strides=2)(residual)
        elif filters != residual.shape[-1]:
            residual = layers.Conv2D(filters, 1)(residual)
        
        x = layers.add([x, residual])
        return x
    
    def _get_callbacks(self, filepath):
        return get_callbacks(filepath=filepath)
