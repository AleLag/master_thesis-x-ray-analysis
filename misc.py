import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np

keras = tf.keras
layers = keras.layers

def draw_plots(history):
    accuracy = history["accuracy"]
    val_accuracy = history["val_accuracy"]
    loss = history["loss"]
    val_loss = history["val_loss"]
    epochs = range(1, len(accuracy) + 1)
    plt.plot(epochs, accuracy, "bo", label="Training accuracy")
    plt.plot(epochs, val_accuracy, "b", label="Validation accuracy")
    plt.title("Training and validation accuracy")
    plt.legend()
    plt.figure()
    plt.plot(epochs, loss, "bo", label="Training loss")
    plt.plot(epochs, val_loss, "b", label="Validation loss")
    plt.title("Training and validation loss")
    plt.legend()
    plt.show()

def get_dataset(dir: str, image_size: tuple, batch_size: int):
    dataset = keras.utils.image_dataset_from_directory(
        directory=dir,
        image_size=image_size,
        batch_size=batch_size
    )
    return dataset

def get_callbacks(filepath):
    return [keras.callbacks.ModelCheckpoint(
        filepath=filepath,
        save_best_only=True,
        monitor="val_loss"
    )]

def fit(model: keras.Model, trainset, epochs, valset, callbacks):
    history = model.fit(
        trainset,
        epochs=epochs,
        validation_data=valset,
        callbacks=callbacks
    )
    return history.history

def get_data_augmentation_layer():
    return keras.Sequential(
        [
            layers.RandomFlip("horizontal"),
            layers.RandomRotation(0.1),
            layers.RandomZoom(0.2)
        ]
    )

def define_conv_base() -> keras.Model:
    return  keras.applications.vgg16.VGG16(
        weights="imagenet",
        include_top=False,
        # input_shape=(180, 180, 3)
    )

def get_img_array(img_path, target_size):
    img = keras.preprocessing.image.load_img(
        img_path, target_size=target_size
    )
    array = keras.preprocessing.image.img_to_array(img)
    array = np.expand_dims(array, axis=0)
    return array