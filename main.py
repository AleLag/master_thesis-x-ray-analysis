import argparse
from prepare_data import runner
from network_model import XRayAnalysisModel
from visualizer import Visualizer

def argument_parser():
    parser = argparse.ArgumentParser(description="CNN Xray Analysis Program")
    parser.add_argument('-bp', '--body_part', dest='body_part', help='Select body ( arm ) part', choices=['elbow', 'finger', 'forearm', 'hand', 'humerus', 'shoulder', 'wrist'], required=True)
    parser.add_argument('-cb', '--conv_base', dest='conv_base', help='Select convolutional base.', choices=['vgg16', 'xception'], required=False, default=None)
    parser.add_argument('--data-augmentation', dest='data_augmentation', action='store_true', default=False)
    parser.add_argument('--feature-extraction', dest='feature_extraction', action='store_true', default=False)
    parser.add_argument('--fine-tunning', dest='fine_tunning', action='store_true', default=False)
    parser.add_argument('-s', '--save-model', dest='model_path', type=str, help='Choose where to save the model', required=True)
    parser.add_argument('-ec', '--epochs-count', dest='epochs', help='Number of epochs', default=30, required=False, type=int)
    parser.add_argument('-v', '--visualize', dest='visualize', help='Choose whether to show visualization effects', action='store_true', default=False)
    parser.add_argument('-hm', '--heatmap', dest='heatmap', help='Choose whether to show heatmap', action='store_true', default=False)

    return parser.parse_args()

def pipeline(args, dataset_dir):
    xray_analyzer = XRayAnalysisModel(body_part=args.body_part, dataset_dir=dataset_dir, conv_base_model=args.conv_base)
    xray_analyzer.init_model(data_augmentation=args.data_augmentation, feature_extration=args.feature_extraction, fine_tunning=args.fine_tunning)
    xray_analyzer.compile_model()
    xray_analyzer.fit_model(epochs=args.epochs, save_model_path=args.model_path)
    test_loss, test_acc = xray_analyzer.evaluate_model(args.model_path)
    print(f'TEST ACC: {test_acc}')
    xray_analyzer.graph()
    xray_analyzer.show_model_summary()

    visualizer = Visualizer(model_path=args.model_path, body_part=args.body_part, dataset_dir=dataset_dir)
    visualizer.show_summary()
    # if args.visaulize:
    #     visualizer.visualize_intermediate_activations()
    #     visualizer.visualize_covnet_filters() # Temporary disabled
    if args.heatmap:
        visualizer.heatmap(f'heatmapped_file_for_{args.body_part.lower()}.png')
    

if __name__== '__main__':
    PATHS_DICTIONARY_MAIN = runner(action=False)
    args = argument_parser()
    pipeline(args, PATHS_DICTIONARY_MAIN)